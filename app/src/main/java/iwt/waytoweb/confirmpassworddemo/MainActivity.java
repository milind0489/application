package iwt.waytoweb.confirmpassworddemo;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    Button click;
    EditText password, confirm;
    TextView get;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        click = (Button) findViewById(R.id.click);
        password = (EditText) findViewById(R.id.password);
        confirm = (EditText) findViewById(R.id.confirmpassword);
        get = (TextView) findViewById(R.id.get);
        click.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String str1 = password.getText().toString();
                String str2 = confirm.getText().toString();
                if (str1.equals(str2)){
                    get.setTextColor(Color.GREEN);
                    Toast.makeText(MainActivity.this, "Password Matched..!", Toast.LENGTH_SHORT).show();
                }else {
                    get.setTextColor(Color.RED);
                    Toast.makeText(MainActivity.this, "Password Do Not Matched", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
