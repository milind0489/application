package iwt.waytoweb.confirmpassworddemo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class CustomSpinner extends AppCompatActivity {
    Spinner spinner;
    TextView set;
    String[] name = {"Milind", "Aayushi", "Avani", "Ruta", "Avani", "Rahul", "Ajay", "Janvi", "Tirth", "Mohit", "Shubham"};
    ArrayAdapter adapter;
    Context context = this;
    LayoutInflater inflater;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_custom_spinner);
        spinner = (Spinner) findViewById(R.id.spinner);
        set = (TextView) findViewById(R.id.set);
        inflater = (LayoutInflater) context.getSystemService(LAYOUT_INFLATER_SERVICE);
        adapter = new ArrayAdapter(context, R.layout.custom_spinner, R.id.text, name);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String s = parent.getItemAtPosition(position) + "";
                set.setText(s);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
